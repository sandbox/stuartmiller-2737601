<?php

function nhs_import_conditions_drush_command() {
  $items  = array();
  $items['import-conditions'] = array(
    'callback'    => 'nhs_import_conditions_setup_batch',
    'description' => dt('Import condition nodes from NHS syndication feed'),
  );
  return $items;
}

function nhs_import_conditions_drush_help($section) {
  switch ($section) {
    case 'drush:nhs_import_conditions':
      return dt("Import condition nodes from NHS syndication feed.");
  }
}
