<?php

/**
 * Helper functions
 */

/**
 * return an array of terms as name => description
 * @return mixed
 */
function get_organisation_terms() {

  if ($vocabulary = taxonomy_vocabulary_machine_name_load('organisation')) {
    if ($terms = taxonomy_get_tree($vocabulary->vid)) {
      foreach ($terms as $term) {
        $options[$term->name] = strip_tags($term->description);
      }
    }
  }

  return $options;
}

/**
 * Generate a type specific feed url.
 * @param $type
 * @return string
 */
function get_feed_url($type) {
  $url = variable_get('nhs_import_base_url', 'http://v1.syndication.nhschoices.nhs.uk/organisations/');

  $postcode = variable_get('nhs_import_base_postcode', 'BN11AA');
  $range = variable_get('nhs_import_range', '13');
  $api_key = variable_get('nhs_import_api_key', 'MRAEUBBU');

  $feed_url = $url . $type . '/postcode/' . $postcode . '.xml?apikey=' . $api_key . '&range=' . $range;

  return $feed_url;
}

/**
 * Generate a type specific staff feed url
 * @param $type
 * @param $org_id
 * @return string
 */
function get_staff_url($type, $org_id) {
  $url = variable_get('nhs_import_base_url', 'http://v1.syndication.nhschoices.nhs.uk/organisations/');
  $api_key = variable_get('nhs_import_api_key', 'MRAEUBBU');

  $feed_url = $url . $type . '/' . $org_id . '/staff.xml?apikey=' . $api_key;

  return $feed_url;
}

/**
 * json encode/decode trick to convert simplexml output to array
 */
function sxml_to_array($object) {
  $json = json_encode($object);
  return json_decode($json);
}

/**
 * Retrieve organisation id from url
 * @param $url
 * @return bool|mixed
 */
function org_id_from_url($url) {
  if ($url) {
    $url_info = parse_url($url);
    return preg_replace('/\D/', '', $url_info['path']);
  }
  else {
    return FALSE;
  }
}

/**
 * Retrieve organisation type from url
 * @param $url
 * @return bool
 */
function org_type_from_url($url) {
  if ($url) {
    $bits = explode("/", $url);
    return $bits[4];
  }
  else {
    return FALSE;
  }
}

/**
 * Check given postcode against include/exclude prefixes
 * @param $postcode
 * @return bool
 */
function nhs_import_postcode_validate($postcode = '') {

  // empty postcode - nothing to compare?
  if (!$postcode) {
    return TRUE;
  }

  // exclude postcodes
  $excludes_source = explode(',', variable_get('nhs_import_postcode_excludes', ''));
  if (is_array($excludes_source)):
    // ensure all array elements are lowercase
    $excludes = nhs_strtolower($excludes_source);
  endif;

  // include postcodes
  $includes_source = explode(',', variable_get('nhs_import_postcode_includes', ''));
  if (is_array($includes_source)):
    // ensure all array elements are lowercase
    $includes = nhs_strtolower($includes_source);
  endif;

  $excludes = isset($excludes) ? $excludes : array();
  $includes = isset($includes) ? $includes : array();

  if (!$excludes && !$includes) {
    return TRUE;
  }

  // prepare postcode var
  $postcode_parts = explode(' ', $postcode);
  $prefix = isset($postcode_parts[0]) ? strtolower($postcode_parts[0]) : NULL;

  // compare with excludes
  if ($excludes && in_array($prefix, $excludes)) {
    return FALSE;
  }

  // compare with includes
  if ($includes && !in_array($prefix, $includes)) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Check for an existing org node
 * - prepare a new node object and return if new
 * @param $org_id
 * @return bool|mixed
 */
function nhs_import_existing_node($org_id) {
  // load by org_id
  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'organisation')
    ->fieldCondition('field_org_id', 'value', $org_id, '=')
    ->range(0,1)
    ->execute();

  if (!empty($entities['node'])) {
    // return existing node
    $node = node_load(current(array_keys($entities['node'])));
    return $node;
  }
  else {
    // create a new node
    $node = new stdClass();
    $node = node_submit($node);
    $node->type = 'organisation';

    return $node;
  }
}

function nhs_strtolower($array) {
  $lower = array();

  foreach ($array as $string) {
    $lower[] = strtolower($string);
  }

  return $lower;
}

function _nhs_import_object_hash($object) {
  return md5(serialize($object));
}

/**
 * retrieve object fields (arrays)
 * @param $object
 * @return bool
 */
function _nhs_import_node_fields($object) {

  $obj_array = array();
  foreach ($object as $key => $value) {
    if (is_array($value)) {
      $obj_array[$key] = $value;
    }
  }
  // ignore this field since it's always unique
  unset($obj_array['rdf_mapping']);

  return $obj_array;
}

/**
 * With a given address, use the googlemaps api to determine the longitude and latitude values
 * @param $address_array
 * @return array|null
 */
function _nhs_import_create_location_data($address_array) {
  $address = array();
  foreach ($address_array as $add) {
    if (!empty($add)) {
      $address[] = str_replace(' ', '+', $add);
    }
  }
  if (count($address) == 1 && $address[0] == $address_array['country']) {
    return NULL;
  }
  $address = implode('+', $address);
  $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";

  $response_data = drupal_http_request($url);
  $response = json_decode($response_data->data);

  $lat = $response->results[0]->geometry->location->lat;
  $lon = $response->results[0]->geometry->location->lng;

  if (!isset($lat) || !isset($lon)) {
    return NULL;
  }
  $location_data = array(
    'wkt' => 'POINT (' . $lon . ' ' . $lat . ')',
    'geo_type' => 'point',
    'lat' => $lat,
    'lon' => $lon,
    'left' => $lon,
    'top' => $lat,
    'right' => $lon,
    'bottom' => $lat,
    'srid' => NULL,
    'accuracy' => NULL,
    'source' => NULL,
  );
  return $location_data;
}