<?php

/**
 * implementation of hook_cron().
 */
function nhs_import_cron() {

  // use DrupalQueue to handle batch processing during cron
  $queue = DrupalQueue::get('nhs_import');

  // retrieve last run stamp
  $last_run = variable_get('nhs_import_cron_last_run', REQUEST_TIME);
  // retrieve import interval in seconds
  $interval = variable_get('nhs_import_cron_interval', 30) * 86400;

  // if current time matches the last run + interval - queue all content
  if (REQUEST_TIME == $last_run + $interval) {

    // queue everything for update
    $new_queue = nhs_import_get_queue();

    if ($new_queue) {
      // log the request time
      variable_set('nhs_import_cron_last_run', REQUEST_TIME);
      watchdog('nhs_import', 'interval expired - adding all organisations to queue for import/update', array(), WATCHDOG_NOTICE);
      foreach ($new_queue as $row) {
        //watchdog('nhs_import', 'added %org_id to the queue for import/update', array('%org_id' => $row['org_id']), WATCHDOG_DEBUG);
        $queue->createItem($row);
      }
    }
  }
}

/**
 * implementation of hook_cron_queue_info()
 * @return array
 */
function nhs_import_cron_queue_info() {
  $queues = array();
  $queues['nhs_import'] = array(
    'worker callback' => 'phaidon_helper_process_resizer_queue',
    'time' => 120,
  );
  return $queues;
}

/**
 * Retrieve all organisations
 * @return array
 */
function nhs_import_get_queue() {
  $terms = get_organisation_terms();
  $queue = array();

  foreach ($terms as $term_name => $term_desc) {
    $urls[] = get_feed_url($term_name);
  }

  // loop through selected urls and retrieve detail urls
  $details_urls = nhs_import_process_urls($urls);

  foreach ($details_urls as $org_id => $urls) {
    // add to queue
    $queue[$org_id] = array($org_id, $urls);
  }

  return $queue;

}