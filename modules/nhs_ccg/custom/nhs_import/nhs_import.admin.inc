<?php

/**
 * Define feed parameters
 * @param $form
 * @param $form_state
 * @return mixed
 */
function nhs_import_admin($form, &$form_state) {

  $form = array();
  $form['admin'] = array(
    '#type' => 'fieldset',
    '#title' => 'NHS Import settings',
  );

  $form['admin']['nhs_import_base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Base URL'),
    '#default_value' => variable_get('nhs_import_base_url', 'http://v1.syndication.nhschoices.nhs.uk/organisations/'),
  );

  $form['admin']['nhs_import_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#size' => 15,
    '#default_value' => variable_get('nhs_import_api_key', 'MRAEUBBU'),
  );

  $form['admin']['nhs_import_base_postcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Base postcode'),
    '#size' => 10,
    '#default_value' => variable_get('nhs_import_base_postcode', 'BN11AA'),
  );

  $form['admin']['nhs_import_range'] = array(
    '#type' => 'textfield',
    '#title' => t('Range (km)'),
    '#size' => 10,
    '#description' => t('Define a range in km from the base postcode'),
    '#default_value' => variable_get('nhs_import_range', '13'),
  );

  $form['admin']['nhs_import_postcode_includes'] = array(
    '#type' => 'textfield',
    '#title' => t('Postcodes to include (eg. BN1)'),
    '#maxlength' => 255,
    '#description' => t('use comma separated values'),
    '#default_value' => variable_get('nhs_import_postcode_includes', 'bn1,bn2,bn3,bn41'),
  );

  $form['admin']['nhs_import_postcode_excludes'] = array(
    '#type' => 'textfield',
    '#title' => t('Postcodes to exclude (eg. BN4)'),
    '#maxlength' => 255,
    '#description' => t('use comma separated values'),
    '#default_value' => variable_get('nhs_import_postcode_excludes', ''),
  );

  if (module_exists('nhs_import_conditions')):
    $form['admin']['nhs_import_conditions_feed_url'] = array(
      '#type' => 'textfield',
      '#title' => 'Conditions feed url',
      '#description' => 'URL to XML feed of A-Z conditions. NOTE: .xml extension is not required.',
      '#default_value' => variable_get('nhs_import_conditions_feed_url', 'http://v1.syndication.nhschoices.nhs.uk/conditions/atoz'),
      '#maxlength'=> 255,
      '#size'=> 100,
    );
  endif;

  $form['admin']['nhs_import_cron_interval'] = array(
    '#type' => 'select',
    '#title' => 'Define import interval (days)',
    '#description' => 'Content will only be imported/updated via cron on these intervals',
    '#options' => drupal_map_assoc(array(7,14,21,30,60)),
    '#default_value' => variable_get('nhs_import_cron_interval', 30),
  );

  return system_settings_form($form);
}