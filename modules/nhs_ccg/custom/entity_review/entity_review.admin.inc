<?php

function entity_review_settings_form($form) {
  $form = array();
  
  $form['review_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Entity Review Settings'),
  );
  
  
  // TODO: add support for other entities 
  $types = field_info_instances('node');
  $review_field = 'field_review_date';
  
  foreach ($types as $key => $type) {
    if (array_key_exists($review_field, $type)):
      $entity_types[$key] = $key;
    endif;
  }
  
  $form['review_settings']['entity_review_settings_bundles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Supported bundles'),
    '#default_value' => variable_get('entity_review_settings_bundles', array('page', 'resource')),
    '#options' => $entity_types,
    '#multiple' => TRUE,
    '#description' => t('Select which supported bundles will be monitored for review.')
  );
  
  $form['review_settings']['entity_review_settings_notify'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify Author?'),
    '#default_value' => variable_get('entity_review_settings_notify', 1),   
  );
  
  $form['review_settings']['entity_review_settings_notify_extra'] = array(
    '#type' => 'textfield',
    '#title' => t('Additionally notify'),
    '#default_value' => variable_get('entity_review_settings_notify_extra', ''),
    '#description' => t('Enter a comma separated list of email addresses to additionally be notified.')
  );
  
  // TODO: make email text configurable with tokens
  // $form['review_settings']['entity_review_settings_email'] = array(
    // '#type' => 'textarea',
    // '#title' => t('Notification email text'),
    // '#default_value' => variable_get('entity_review_settings_email', ''),
    // '#description' => t('Enter the email content for notiying users that content has expired.'), 
  // );
  
  for($i=1;$i<48;$i++) {
    $options[$i] = $i;
  }
  
  $form['review_settings']['entity_review_settings_period'] = array(
    '#type' => 'select',
    '#title' => t('Notifcation period'),
    '#default_value' => variable_get('eneity_review_settings_period', 28),
    '#description' => t('Define the period (in days) prior to the review date that a notifcation email should be triggered.'),
    '#options' => $options,
  );
  
  $form['review_settings']['entity_review_settings_default_date'] = array(
    '#type' => 'date',
    '#title' => t('Default review date'),
    '#default_value' => variable_get('entity_review_settings_default_date', array('year' => 2014, 'month' => 3, 'day' => 31)),
    '#description' => t('If review date field is empty use this date to compare review status.')
  );
  
  return system_settings_form($form);
}
