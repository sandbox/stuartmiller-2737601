// Global JS

(function($){

  $(function() {

    if( $('#nhs-choices-widget').length > 0 ) {
      $('#nhs-choices-widget ul').children('li').each(function() {
        $(this).children('.widget-search-form').hide();
      });

      $('#nhs-choices-widget ul li').children('h3').each(function() {
        $(this).bind('click',function() {
          $(this).parent().siblings('li').children('h3').removeClass('active-choice');
          $(this).parent().siblings('li').children('.widget-search-form').slideUp(300);
          $(this).toggleClass('active-choice');
          $(this).next('.widget-search-form').slideToggle(300);
        });
      });
    }

  });

})(jQuery);