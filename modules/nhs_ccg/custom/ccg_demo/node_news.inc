<?php

/**
 * @file
 *  Migrations for CCG Demo News Nodes.
 */

class CCGDemoNewsMigration extends Migration {

  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import CCG news nodes');
    $import_path = drupal_get_path('module', 'ccg_demo') . '/import/';

    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'title' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'ccg_demo.nodes.news.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('news');

    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('body', 'body');
    $this->addFieldMapping('body:format')->defaultValue('filtered_html');
    $this->addFieldMapping('field_tags', 'news_type');

  }

  function csvcolumns() {
    $columns[0] = array('title', 'title');
    $columns[1] = array('body', 'body');
    $columns[2] = array('news_type', 'news_type');
    return $columns;
  }

}
