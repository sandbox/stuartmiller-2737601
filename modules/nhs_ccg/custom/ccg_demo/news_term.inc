<?php

class CCGDemoNewsTermMigration extends Migration {

  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Migrate taxonomy terms for the news vocabulary.');
    $import_path = drupal_get_path('module', 'ccg_demo') . '/import/';

    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'name' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'ccg_demo.taxonomy.news.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('news');

    $this->addFieldMapping('name', 'name');

  }

  function csvcolumns() {
    $columns[0] = array('name', 'name');
    return $columns;
  }

}
