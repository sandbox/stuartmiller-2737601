<?php

/**
 * @file
 * Declares our migrations.
 */


/**
 * Implements hook_migrate_api().
 */
function ccg_demo_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'ccg' => array(
        'title' => t('CCG Demo Migrations'),
      ),
    ),
    'migrations' => array(
      'CCGDemoBasicPage' => array(
        'class_name' => 'CCGDemoBasicPageMigration',
        'group_name' => 'ccg',
        'dependencies' => array(
          'CCGDemoFiles'
        )
      ),
      'CCGDemoNews' => array(
        'class_name' => 'CCGDemoNewsMigration',
        'group_name' => 'ccg',
        'dependencies' => array(
          'CCGDemoNewsTerms'
        )
      ),
      'CCGDemoMainMenu' => array(
        'class_name' => 'CCGDemoMainMenuMigration',
        'group_name' => 'ccg',
        'dependencies' => array(
          'CCGDemoBasicPage',
        )
      ),
      'CCGDemoNewsTerms' => array(
        'class_name' => 'CCGDemoNewsTermMigration',
        'group_name' => 'ccg',
      ),
      'CCGDemoFiles' => array(
        'class_name' => 'CCGDemoFileMigration',
        'group_name' => 'ccg',
      ),
    ),
  );
  return $api;
}
