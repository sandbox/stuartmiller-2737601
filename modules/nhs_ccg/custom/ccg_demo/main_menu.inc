<?php

/**
 * @file
 *  Migrations for CCG Demo Basic Page Nodes.
 */

class CCGDemoMainMenuMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import CCG main menu');
    $import_path = drupal_get_path('module', 'ccg_demo') . '/import/';

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'mlid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'ID of destination link',
        ),
      ),
      MigrateDestinationMenuLinks::getKeySchema()
    );

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV($import_path . 'ccg_demo.main_menu.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationMenuLinks();

    $this->addFieldMapping('menu_name')->defaultValue('main-menu'); // Always Main menu
    $this->addFieldMapping('link_path', 'path');          // path of the link
    $this->addFieldMapping('router_path')->defaultValue('node/%');
    $this->addFieldMapping('link_title', 'title');        // Title of the menu item
    $this->addFieldMapping('external')->defaultValue('0'); // Internal
    $this->addFieldMapping('expanded')->defaultValue('0');
    $this->addFieldMapping('weight','mlid');            // weight
    $this->addFieldMapping('customized')->defaultValue('1'); // not customized
    $this->addFieldMapping('has_children')->defaultValue('0');  // Will be overridden automatically
    $this->addFieldMapping('depth')->defaultValue('1'); // Will be overridden automatically

    $this->addUnmigratedDestinations(array('module', 'hidden','options','p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9', 'updated'));
  }

  function prepareRow($row) {
    // Convert the alias to the node path.
    $node_path = drupal_lookup_path('source', $row->path);

    if(FALSE == $node_path)
    {
      return FALSE;
    }

    $row->path = $node_path;
  }

  function csvcolumns() {
    $columns[0] = array('mlid', 'mlid');
    $columns[1] = array('title', 'title');
    $columns[2] = array('path', 'path');
    return $columns;
  }

}
