<?php

class CCGDemoFileMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import CCG files');
    $import_path = drupal_get_path('module', 'ccg_demo') . '/import/';

    $this->source = new MigrateSourceCSV($import_path . 'ccg_demo.files.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationFile();

    $this->map = new MigrateSQLMap($this->machineName,
        array('fileid' => array(
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
              'description' => 'File ID.'
              )
           ),
        MigrateDestinationFile::getKeySchema()
    );

    $this->addFieldMapping('source_dir')->defaultValue($import_path . 'images');
    $this->addFieldMapping('value', 'url');

    $this->addUnmigratedDestinations(array(
      'destination_dir',
      'destination_file',
      'fid',
      'file_replace',
      'preserve_files',
      'timestamp',
      'uid',
      'urlencode',
    ));

    $this->removeFieldMapping('pathauto');
  }

  function csvcolumns() {
    $columns[0] = array('fileid', 'fileid');
    $columns[1] = array('url', 'url');
    return $columns;
  }

}
