<?php
/**
 * @file
 * nhs_youtube_video.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function nhs_youtube_video_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'content_related_youtube_videos';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Content related Youtube Videos';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['css_class'] = 'related-youtube-videos-block';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_field_content_reference_node']['id'] = 'reverse_field_content_reference_node';
  $handler->display->display_options['relationships']['reverse_field_content_reference_node']['table'] = 'node';
  $handler->display->display_options['relationships']['reverse_field_content_reference_node']['field'] = 'reverse_field_content_reference_node';
  $handler->display->display_options['relationships']['reverse_field_content_reference_node']['label'] = 'Referencing Entity';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['relationship'] = 'reverse_field_content_reference_node';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Youtube Video */
  $handler->display->display_options['fields']['field_youtube_video']['id'] = 'field_youtube_video';
  $handler->display->display_options['fields']['field_youtube_video']['table'] = 'field_data_field_youtube_video';
  $handler->display->display_options['fields']['field_youtube_video']['field'] = 'field_youtube_video';
  $handler->display->display_options['fields']['field_youtube_video']['relationship'] = 'reverse_field_content_reference_node';
  $handler->display->display_options['fields']['field_youtube_video']['label'] = '';
  $handler->display->display_options['fields']['field_youtube_video']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_youtube_video']['element_class'] = 'content-[nid]';
  $handler->display->display_options['fields']['field_youtube_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_youtube_video']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_youtube_video']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_youtube_video']['settings'] = array(
    'file_view_mode' => 'media_large',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'reverse_field_content_reference_node';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_type'] = 'div';
  $handler->display->display_options['fields']['title']['element_class'] = 'title-[nid]';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['relationship'] = 'reverse_field_content_reference_node';
  $handler->display->display_options['filters']['type']['value'] = array(
    'video' => 'video',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;

  /* Display: Homepage */
  $handler = $view->new_display('block', 'Homepage', 'homepage_video_block');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'related-youtube-videos-block grid-4 alpha omega';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['relationship'] = 'reverse_field_content_reference_node';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Youtube Video */
  $handler->display->display_options['fields']['field_youtube_video']['id'] = 'field_youtube_video';
  $handler->display->display_options['fields']['field_youtube_video']['table'] = 'field_data_field_youtube_video';
  $handler->display->display_options['fields']['field_youtube_video']['field'] = 'field_youtube_video';
  $handler->display->display_options['fields']['field_youtube_video']['relationship'] = 'reverse_field_content_reference_node';
  $handler->display->display_options['fields']['field_youtube_video']['label'] = '';
  $handler->display->display_options['fields']['field_youtube_video']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_youtube_video']['element_class'] = 'content-[nid]';
  $handler->display->display_options['fields']['field_youtube_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_youtube_video']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_youtube_video']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_youtube_video']['settings'] = array(
    'file_view_mode' => 'homepage',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'reverse_field_content_reference_node';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_type'] = 'div';
  $handler->display->display_options['fields']['title']['element_class'] = 'title-[nid]';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  $export['content_related_youtube_videos'] = $view;

  return $export;
}
