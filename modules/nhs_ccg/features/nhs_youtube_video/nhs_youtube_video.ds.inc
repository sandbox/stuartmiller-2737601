<?php
/**
 * @file
 * nhs_youtube_video.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function nhs_youtube_video_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'homepage';
  $ds_view_mode->label = 'Homepage';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['homepage'] = $ds_view_mode;

  return $export;
}
