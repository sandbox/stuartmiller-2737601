<?php
/**
 * @file
 * nhs_site_default.box.inc
 */

/**
 * Implements hook_default_box().
 */
function nhs_site_default_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'copywrite_notice';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Copywrite Notice';
  $box->options = array(
    'body' => array(
      'value' => '<p>
Copyright <sup>&copy;</sup>&nbsp;<a href="http://miggle.co.uk" title="Web design and development by miggle.co.uk, Brighton and Hove" style="line-height: 1.5em;">miggle.co.uk</a>
</p>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['copywrite_notice'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'fs_widget_home_block';
  $box->plugin_key = 'simple';
  $box->title = 'Find your nearest';
  $box->description = 'Display FS Widget on the home page';
  $box->options = array(
    'body' => array(
      'value' => '',
      'format' => 'filtered_html',
    ),
    'additional_classes' => '',
  );
  $export['fs_widget_home_block'] = $box;

  return $export;
}
