<?php
/**
 * @file
 * nhs_site_default.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function nhs_site_default_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "delta" && $api == "delta") {
    return array("version" => "3");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
  if ($module == "linkit" && $api == "linkit_profiles") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function nhs_site_default_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function nhs_site_default_image_default_styles() {
  $styles = array();

  // Exported image style: nhs_article_header_image.
  $styles['nhs_article_header_image'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 338,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'nhs_article_header_image',
  );

  // Exported image style: nhs_article_list.
  $styles['nhs_article_list'] = array(
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 100,
          'height' => 90,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'nhs_article_list',
  );

  return $styles;
}
