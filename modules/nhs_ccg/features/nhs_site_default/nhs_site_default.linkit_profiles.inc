<?php
/**
 * @file
 * nhs_site_default.linkit_profiles.inc
 */

/**
 * Implements hook_default_linkit_profiles().
 */
function nhs_site_default_default_linkit_profiles() {
  $export = array();

  $linkit_profile = new LinkitProfile();
  $linkit_profile->disabled = FALSE; /* Edit this to true to make a default linkit_profile disabled initially */
  $linkit_profile->api_version = 1;
  $linkit_profile->name = 'link_it';
  $linkit_profile->admin_title = 'Link It';
  $linkit_profile->admin_description = '';
  $linkit_profile->profile_type = '1';
  $linkit_profile->data = array(
    'text_formats' => array(
      'filtered_html' => 'filtered_html',
      'full_html' => 'full_html',
      'plain_text' => 0,
    ),
    'search_plugins' => array(
      'entity:taxonomy_term' => array(
        'enabled' => 1,
        'weight' => '-10',
      ),
      'entity:node' => array(
        'enabled' => 1,
        'weight' => '-10',
      ),
      'entity:comment' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:file' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:user' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
    ),
    'entity:comment' => array(
      'result_description' => '',
      'bundles' => array(
        'comment_node_article' => 0,
        'comment_node_page' => 0,
        'comment_node_carousel_promotion' => 0,
        'comment_node_condition' => 0,
        'comment_node_dentist' => 0,
        'comment_node_external_link' => 0,
        'comment_node_general_practitioner' => 0,
        'comment_node_key_staff_member' => 0,
        'comment_node_nhs_home_page' => 0,
        'comment_node_news' => 0,
        'comment_node_opticians' => 0,
        'comment_node_organisation' => 0,
        'comment_node_pharmacy' => 0,
        'comment_node_rating' => 0,
        'comment_node_resource' => 0,
        'comment_node_staff' => 0,
        'comment_node_video' => 0,
        'comment_node_webform' => 0,
      ),
      'group_by_bundle' => 0,
    ),
    'entity:node' => array(
      'result_description' => '',
      'bundles' => array(
        'article' => 0,
        'page' => 0,
        'carousel_promotion' => 0,
        'condition' => 0,
        'dentist' => 0,
        'external_link' => 0,
        'general_practitioner' => 0,
        'key_staff_member' => 0,
        'nhs_home_page' => 0,
        'news' => 0,
        'opticians' => 0,
        'organisation' => 0,
        'pharmacy' => 0,
        'rating' => 0,
        'resource' => 0,
        'staff' => 0,
        'video' => 0,
        'webform' => 0,
      ),
      'group_by_bundle' => 0,
      'include_unpublished' => 0,
    ),
    'entity:file' => array(
      'result_description' => '',
      'bundles' => array(
        'audio' => 0,
        'document' => 0,
        'image' => 0,
        'video' => 0,
      ),
      'group_by_bundle' => 0,
      'show_scheme' => 0,
      'group_by_scheme' => 0,
      'url_type' => 'entity',
      'image_extra_info' => array(
        'thumbnail' => 'thumbnail',
        'dimensions' => 'dimensions',
      ),
    ),
    'entity:taxonomy_term' => array(
      'result_description' => '',
      'bundles' => array(
        'tags' => 0,
        'news' => 0,
        'resources' => 0,
        'key_staff_members' => 0,
        'organisation' => 0,
        'staff_type' => 0,
      ),
      'group_by_bundle' => 0,
    ),
    'entity:user' => array(
      'result_description' => '',
    ),
    'insert_plugin' => array(
      'url_method' => '2',
    ),
    'attribute_plugins' => array(
      'class' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'id' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'rel' => array(
        'enabled' => 1,
        'weight' => '-10',
      ),
      'target' => array(
        'enabled' => 1,
        'weight' => '-10',
      ),
      'title' => array(
        'enabled' => 1,
        'weight' => '-10',
      ),
      'accesskey' => array(
        'enabled' => 1,
        'weight' => '-10',
      ),
    ),
    'autocomplete' => array(
      'charLimit' => '3',
      'wait' => '350',
      'remoteTimeout' => '10000',
    ),
  );
  $export['link_it'] = $linkit_profile;

  return $export;
}
