<?php
/**
 * @file
 * nhs_site_default.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function nhs_site_default_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nhs_global_home_settings';
  $context->description = 'Set the correct theme delta for home page & blocks';
  $context->tag = 'nhs';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'search-form' => array(
          'module' => 'search',
          'delta' => 'form',
          'region' => 'menu',
          'weight' => '-10',
        ),
        'nhs_global_choice_widget-nhs_global_choice_widget' => array(
          'module' => 'nhs_global_choice_widget',
          'delta' => 'nhs_global_choice_widget',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-nhs_news-block_1' => array(
          'module' => 'views',
          'delta' => 'nhs_news-block_1',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'twitter_block-1' => array(
          'module' => 'twitter_block',
          'delta' => '1',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
        'boxes-copywrite_notice' => array(
          'module' => 'boxes',
          'delta' => 'copywrite_notice',
          'region' => 'footer_first',
          'weight' => '-10',
        ),
        'menu-menu-footer-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-footer-menu',
          'region' => 'footer_first',
          'weight' => '-9',
        ),
        'widgets-s_socialmedia_profile-default' => array(
          'module' => 'widgets',
          'delta' => 's_socialmedia_profile-default',
          'region' => 'footer_second',
          'weight' => '-10',
        ),
        'views-nhs_home_blocks-block' => array(
          'module' => 'views',
          'delta' => 'nhs_home_blocks-block',
          'region' => 'content_welcome_profile',
          'weight' => '-10',
        ),
        'views-nhs_home_blocks-block_1' => array(
          'module' => 'views',
          'delta' => 'nhs_home_blocks-block_1',
          'region' => 'content_your_say',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'nhs_home_theming',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Set the correct theme delta for home page & blocks');
  t('nhs');
  $export['nhs_global_home_settings'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nhs_global_theme';
  $context->description = 'Context to set global theme sitewide';
  $context->tag = 'nhs';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
        '~node/7' => '~node/7',
      ),
    ),
  );
  $context->reactions = array(
    'delta' => array(
      'delta_template' => 'nhs_global_theme',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Context to set global theme sitewide');
  t('nhs');
  $export['nhs_global_theme'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nhs_in_this_section';
  $context->description = 'Display child pages of the current section';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
        '~node/7' => '~node/7',
        '~/contact-us/*' => '~/contact-us/*',
        '~contact-us/*' => '~contact-us/*',
        '~node/6' => '~node/6',
        '~/news/*' => '~/news/*',
        '~news/*' => '~news/*',
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu_block-1' => array(
          'module' => 'menu_block',
          'delta' => '1',
          'region' => 'content',
          'weight' => '8',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Display child pages of the current section');
  $export['nhs_in_this_section'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nhs_node_review_block';
  $context->description = 'Correctly show the node review block';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
        '~node/6' => '~node/6',
        '~news' => '~news',
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'entity_review_block-entity_review_block' => array(
          'module' => 'entity_review_block',
          'delta' => 'entity_review_block',
          'region' => 'content',
          'weight' => '9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Correctly show the node review block');
  $export['nhs_node_review_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nhs_site_default';
  $context->description = 'Default site settings';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '0',
        ),
        'views-089d5fc80c480e4ca2057c6c62a95dc0' => array(
          'module' => 'views',
          'delta' => '089d5fc80c480e4ca2057c6c62a95dc0',
          'region' => 'content',
          'weight' => '1',
        ),
        'views-nhs_addthis-block' => array(
          'module' => 'views',
          'delta' => 'nhs_addthis-block',
          'region' => 'content',
          'weight' => '3',
        ),
        'node_comment_block-node_comments' => array(
          'module' => 'node_comment_block',
          'delta' => 'node_comments',
          'region' => 'content',
          'weight' => '10',
        ),
        'boxes-copywrite_notice' => array(
          'module' => 'boxes',
          'delta' => 'copywrite_notice',
          'region' => 'footer_first',
          'weight' => '-10',
        ),
        'menu-menu-footer-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-footer-menu',
          'region' => 'footer_first',
          'weight' => '-9',
        ),
        'widgets-s_socialmedia_profile-default' => array(
          'module' => 'widgets',
          'delta' => 's_socialmedia_profile-default',
          'region' => 'footer_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Default site settings');
  $export['nhs_site_default'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nhs_site_sidebar_sitewide';
  $context->description = 'Right hand sidebar display';
  $context->tag = 'nhs';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
        '~node/7' => '~node/7',
        '~search/*' => '~search/*',
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'search-form' => array(
          'module' => 'search',
          'delta' => 'form',
          'region' => 'menu',
          'weight' => '-10',
        ),
        'nhs_global_choice_widget-nhs_global_choice_widget' => array(
          'module' => 'nhs_global_choice_widget',
          'delta' => 'nhs_global_choice_widget',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-nhs_related_links-block' => array(
          'module' => 'views',
          'delta' => 'nhs_related_links-block',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'views-sidebar_resources-block' => array(
          'module' => 'views',
          'delta' => 'sidebar_resources-block',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
        'views-clone_of_related_policies-block' => array(
          'module' => 'views',
          'delta' => 'clone_of_related_policies-block',
          'region' => 'sidebar_second',
          'weight' => '-6',
        ),
        'views-related_downloads-block' => array(
          'module' => 'views',
          'delta' => 'related_downloads-block',
          'region' => 'sidebar_second',
          'weight' => '-5',
        ),
        'views-related_minutes_and_notes-block' => array(
          'module' => 'views',
          'delta' => 'related_minutes_and_notes-block',
          'region' => 'sidebar_second',
          'weight' => '-4',
        ),
        'twitter_block-1' => array(
          'module' => 'twitter_block',
          'delta' => '1',
          'region' => 'sidebar_second',
          'weight' => '-3',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'nhs_global_theme',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Right hand sidebar display');
  t('nhs');
  $export['nhs_site_sidebar_sitewide'] = $context;

  return $export;
}
