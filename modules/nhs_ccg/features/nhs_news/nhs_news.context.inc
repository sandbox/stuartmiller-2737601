<?php
/**
 * @file
 * nhs_news.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function nhs_news_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nhs_doctors_note_hub';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/17' => 'node/17',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-nhs_doctors_note-block' => array(
          'module' => 'views',
          'delta' => 'nhs_doctors_note-block',
          'region' => 'content',
          'weight' => '2',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['nhs_doctors_note_hub'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nhs_news_hub';
  $context->description = 'Display the latest news story';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'news' => 'news',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-nhs_news-block' => array(
          'module' => 'views',
          'delta' => 'nhs_news-block',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Display the latest news story');
  $export['nhs_news_hub'] = $context;

  return $export;
}
