<?php
/**
 * @file
 * nhs_full_width_pages.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function nhs_full_width_pages_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nhs_full_width';
  $context->description = 'Content area is full width with no sidebar';
  $context->tag = 'nhs';
  $context->conditions = array();
  $context->reactions = array(
    'delta' => array(
      'delta_template' => 'nhs_full_width',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content area is full width with no sidebar');
  t('nhs');
  $export['nhs_full_width'] = $context;

  return $export;
}
