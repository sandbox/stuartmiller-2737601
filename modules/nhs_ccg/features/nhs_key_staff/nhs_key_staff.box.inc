<?php
/**
 * @file
 * nhs_key_staff.box.inc
 */

/**
 * Implements hook_default_box().
 */
function nhs_key_staff_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'registered_interests_link';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'A link to the Registered Interests page';
  $box->options = array(
    'body' => array(
      'value' => '<div id="registered-interests-link"><p>To see a list of our members registered interests, please <a href="/who-we-are/registered-interests">Click here</a></p></div>',
      'format' => 'full_html',
    ),
  );
  $export['registered_interests_link'] = $box;

  return $export;
}
