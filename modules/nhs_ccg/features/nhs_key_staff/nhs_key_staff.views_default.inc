<?php
/**
 * @file
 * nhs_key_staff.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function nhs_key_staff_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'key_staff';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Key Staff';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = '<none>';
  $handler->display->display_options['css_class'] = 'block-views-key-staff-block';
  $handler->display->display_options['hide_admin_links'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'key_staff_member_view',
    'image_link' => 'content',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Qualifications */
  $handler->display->display_options['fields']['field_qualifications']['id'] = 'field_qualifications';
  $handler->display->display_options['fields']['field_qualifications']['table'] = 'field_data_field_qualifications';
  $handler->display->display_options['fields']['field_qualifications']['field'] = 'field_qualifications';
  $handler->display->display_options['fields']['field_qualifications']['label'] = '';
  $handler->display->display_options['fields']['field_qualifications']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['field_title']['id'] = 'field_title';
  $handler->display->display_options['fields']['field_title']['table'] = 'field_data_field_title';
  $handler->display->display_options['fields']['field_title']['field'] = 'field_title';
  $handler->display->display_options['fields']['field_title']['label'] = '';
  $handler->display->display_options['fields']['field_title']['element_label_colon'] = FALSE;
  /* Field: Content: Bio */
  $handler->display->display_options['fields']['field_bio']['id'] = 'field_bio';
  $handler->display->display_options['fields']['field_bio']['table'] = 'field_data_field_bio';
  $handler->display->display_options['fields']['field_bio']['field'] = 'field_bio';
  $handler->display->display_options['fields']['field_bio']['label'] = '';
  $handler->display->display_options['fields']['field_bio']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_bio']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['field_bio']['settings'] = array(
    'trim_length' => '200',
  );
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['element_class'] = 'read-more';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['text'] = 'Read more...';
  /* Sort criterion: Weight: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'weight_weights';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'key_staff_member' => 'key_staff_member',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Filter criterion: Content: Member Type (field_member_type) */
  $handler->display->display_options['filters']['field_member_type_tid']['id'] = 'field_member_type_tid';
  $handler->display->display_options['filters']['field_member_type_tid']['table'] = 'field_data_field_member_type';
  $handler->display->display_options['filters']['field_member_type_tid']['field'] = 'field_member_type_tid';
  $handler->display->display_options['filters']['field_member_type_tid']['value'] = array(
    0 => '10',
  );
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['operator_id'] = 'field_member_type_tid_op';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['label'] = 'Member Type (field_member_type)';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['operator'] = 'field_member_type_tid_op';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['identifier'] = 'field_member_type_tid';
  $handler->display->display_options['filters']['field_member_type_tid']['vocabulary'] = 'key_staff_members';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'key_staff_member' => 'key_staff_member',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Filter criterion: Content: Member Type (field_member_type) */
  $handler->display->display_options['filters']['field_member_type_tid']['id'] = 'field_member_type_tid';
  $handler->display->display_options['filters']['field_member_type_tid']['table'] = 'field_data_field_member_type';
  $handler->display->display_options['filters']['field_member_type_tid']['field'] = 'field_member_type_tid';
  $handler->display->display_options['filters']['field_member_type_tid']['value'] = array(
    0 => '9',
  );
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['operator_id'] = 'field_member_type_tid_op';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['label'] = 'Member Type (field_member_type)';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['operator'] = 'field_member_type_tid_op';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['identifier'] = 'field_member_type_tid';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_member_type_tid']['vocabulary'] = 'key_staff_members';
  $handler->display->display_options['block_description'] = 'Key Staff Director';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'key_staff_member' => 'key_staff_member',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Filter criterion: Content: Member Type (field_member_type) */
  $handler->display->display_options['filters']['field_member_type_tid']['id'] = 'field_member_type_tid';
  $handler->display->display_options['filters']['field_member_type_tid']['table'] = 'field_data_field_member_type';
  $handler->display->display_options['filters']['field_member_type_tid']['field'] = 'field_member_type_tid';
  $handler->display->display_options['filters']['field_member_type_tid']['value'] = array(
    0 => '10',
  );
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['operator_id'] = 'field_member_type_tid_op';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['label'] = 'Member Type (field_member_type)';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['operator'] = 'field_member_type_tid_op';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['identifier'] = 'field_member_type_tid';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_member_type_tid']['vocabulary'] = 'key_staff_members';
  $handler->display->display_options['block_description'] = 'Key Staff Board Member';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_2');
  $handler->display->display_options['defaults']['footer'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'key_staff_member' => 'key_staff_member',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Filter criterion: Content: Member Type (field_member_type) */
  $handler->display->display_options['filters']['field_member_type_tid']['id'] = 'field_member_type_tid';
  $handler->display->display_options['filters']['field_member_type_tid']['table'] = 'field_data_field_member_type';
  $handler->display->display_options['filters']['field_member_type_tid']['field'] = 'field_member_type_tid';
  $handler->display->display_options['filters']['field_member_type_tid']['value'] = array(
    0 => '11',
  );
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['operator_id'] = 'field_member_type_tid_op';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['label'] = 'Member Type (field_member_type)';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['operator'] = 'field_member_type_tid_op';
  $handler->display->display_options['filters']['field_member_type_tid']['expose']['identifier'] = 'field_member_type_tid';
  $handler->display->display_options['filters']['field_member_type_tid']['vocabulary'] = 'key_staff_members';
  $handler->display->display_options['block_description'] = 'Key Staff Lay Member';
  $export['key_staff'] = $view;

  $view = new view();
  $view->name = 'registered_interests';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Registered Interests';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Registered Interests';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Registered Interests */
  $handler->display->display_options['fields']['field_registered_interests']['id'] = 'field_registered_interests';
  $handler->display->display_options['fields']['field_registered_interests']['table'] = 'field_data_field_registered_interests';
  $handler->display->display_options['fields']['field_registered_interests']['field'] = 'field_registered_interests';
  $handler->display->display_options['fields']['field_registered_interests']['label'] = '';
  $handler->display->display_options['fields']['field_registered_interests']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_registered_interests']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_registered_interests']['multi_type'] = 'ul';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Registered Interests (field_registered_interests) */
  $handler->display->display_options['arguments']['field_registered_interests_value']['id'] = 'field_registered_interests_value';
  $handler->display->display_options['arguments']['field_registered_interests_value']['table'] = 'field_data_field_registered_interests';
  $handler->display->display_options['arguments']['field_registered_interests_value']['field'] = 'field_registered_interests_value';
  $handler->display->display_options['arguments']['field_registered_interests_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_registered_interests_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_registered_interests_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_registered_interests_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_registered_interests_value']['limit'] = '0';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'key_staff_member' => 'key_staff_member',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Registered Interests (field_registered_interests:delta) */
  $handler->display->display_options['filters']['delta']['id'] = 'delta';
  $handler->display->display_options['filters']['delta']['table'] = 'field_data_field_registered_interests';
  $handler->display->display_options['filters']['delta']['field'] = 'delta';
  $handler->display->display_options['filters']['delta']['operator'] = 'not empty';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'who-we-are/registered-interests';
  $export['registered_interests'] = $view;

  return $export;
}
