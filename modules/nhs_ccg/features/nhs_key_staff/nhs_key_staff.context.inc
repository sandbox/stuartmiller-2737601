<?php
/**
 * @file
 * nhs_key_staff.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function nhs_key_staff_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nhs_key_staff';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/20' => 'node/20',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-key_staff-block' => array(
          'module' => 'views',
          'delta' => 'key_staff-block',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-key_staff-block_1' => array(
          'module' => 'views',
          'delta' => 'key_staff-block_1',
          'region' => 'content',
          'weight' => '-9',
        ),
        'views-key_staff-block_2' => array(
          'module' => 'views',
          'delta' => 'key_staff-block_2',
          'region' => 'content',
          'weight' => '-8',
        ),
        'boxes-registered_interests_link' => array(
          'module' => 'boxes',
          'delta' => 'registered_interests_link',
          'region' => 'content',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['nhs_key_staff'] = $context;

  return $export;
}
