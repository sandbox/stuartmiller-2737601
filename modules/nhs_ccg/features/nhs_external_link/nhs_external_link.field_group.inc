<?php
/**
 * @file
 * nhs_external_link.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function nhs_external_link_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_external_link_section|node|external_link|form';
  $field_group->group_name = 'group_external_link_section';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'external_link';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Link Section',
    'weight' => '5',
    'children' => array(
      0 => 'field_external_linked_content',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_external_link_section|node|external_link|form'] = $field_group;

  return $export;
}
