<?php
/**
 * @file
 * nhs_external_link.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function nhs_external_link_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function nhs_external_link_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function nhs_external_link_node_info() {
  $items = array(
    'external_link' => array(
      'name' => t('External Link'),
      'base' => 'node_content',
      'description' => t('Create an external link to be used in content & added to links pages'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
