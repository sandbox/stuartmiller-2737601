<?php
/**
 * @file
 * nhs_homepage_carousel.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function nhs_homepage_carousel_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nhs_homepage_carousel';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-homepage_carousel-block' => array(
          'module' => 'views',
          'delta' => 'homepage_carousel-block',
          'region' => 'content_welcome_profile',
          'weight' => '-34',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['nhs_homepage_carousel'] = $context;

  return $export;
}
