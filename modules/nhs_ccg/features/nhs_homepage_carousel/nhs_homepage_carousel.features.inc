<?php
/**
 * @file
 * nhs_homepage_carousel.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function nhs_homepage_carousel_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function nhs_homepage_carousel_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function nhs_homepage_carousel_image_default_styles() {
  $styles = array();

  // Exported image style: homepage_image.
  $styles['homepage_image'] = array(
    'name' => 'homepage_image',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 812,
          'height' => 287,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function nhs_homepage_carousel_node_info() {
  $items = array(
    'carousel_promotion' => array(
      'name' => t('Carousel Promotion'),
      'base' => 'node_content',
      'description' => t('Add an item to be displayed in the promotional carousel on the homepage.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
