<?php
/**
 * @file
 * nhs_contact_us_page.box.inc
 */

/**
 * Implements hook_default_box().
 */
function nhs_contact_us_page_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'contact_us_map';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'NHS Map';
  $box->options = array(
    'body' => array(
      'value' => '<iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=BN1+4FU&amp;aq=&amp;sll=52.8382,-2.327815&amp;sspn=10.468665,24.521484&amp;ie=UTF8&amp;hq=&amp;hnear=BN1+4FU,+United+Kingdom&amp;t=m&amp;ll=50.829252,-0.139539&amp;spn=0.004744,0.00912&amp;z=16&amp;iwloc=&amp;output=embed"></iframe><br /><small><a href="https://maps.google.co.uk/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=BN1+4FU&amp;aq=&amp;sll=52.8382,-2.327815&amp;sspn=10.468665,24.521484&amp;ie=UTF8&amp;hq=&amp;hnear=BN1+4FU,+United+Kingdom&amp;t=m&amp;ll=50.829252,-0.139539&amp;spn=0.004744,0.00912&amp;z=16&amp;iwloc=A">View Larger Map</a></small>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['contact_us_map'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'nhs_foi';
  $box->plugin_key = 'simple';
  $box->title = 'Freedom of Information requests (FOI)';
  $box->description = 'Freedom of Information requests';
  $box->options = array(
    'body' => array(
      'value' => '<p><span style="line-height: 1.538em;">If you wish to make a request under the Freedom of Information Act, please e-mail <a href="mailto:bhccg.foi@nhs.net">bhccg.foi@nhs.net</a> or write to:</span></p><p>FOI Co-ordinator<br />Brighton and Hove CCG<br />Level 3<br />Lanchester House<br />Trafalgar Place<br />Brighton<br />BN1 4FU</p><p>For further information on making an FOI request, please <a href="node/35">click here</a>.</p>',
      'format' => 'filtered_html',
    ),
  );
  $export['nhs_foi'] = $box;

  return $export;
}
