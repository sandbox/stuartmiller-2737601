<?php
/**
 * @file
 * nhs_contact_us_page.delta.inc
 */

/**
 * Implements hook_delta_default_templates().
 */
function nhs_contact_us_page_delta_default_templates() {
  $export = array();

  $delta = new stdClass();
  $delta->disabled = FALSE; /* Edit this to true to make a default delta disabled initially */
  $delta->api_version = 3;
  $delta->machine_name = 'nhs_two_column_clean';
  $delta->name = 'NHS Two Column - Clean';
  $delta->description = 'Two column clean layout';
  $delta->theme = 'nhs';
  $delta->mode = 'preserve';
  $delta->parent = '';
  $delta->settings = array(
    'theme_nhs_settings' => array(
      'alpha_css' => array(
        'alpha-reset.css' => 'alpha-reset.css',
        'alpha-mobile.css' => 'alpha-mobile.css',
        'alpha-alpha.css' => 'alpha-alpha.css',
        'omega-text.css' => 'omega-text.css',
        'omega-branding.css' => 'omega-branding.css',
        'omega-menu.css' => 'omega-menu.css',
        'omega-forms.css' => 'omega-forms.css',
        'omega-visuals.css' => 'omega-visuals.css',
        'global.css' => 'global.css',
        'widgets/nhs.fs.widget.css' => 0,
      ),
      'alpha_region_content_footer_block_second_force' => 0,
      'alpha_region_content_footer_block_second_prefix' => '0',
      'alpha_region_content_footer_block_second_columns' => '1',
      'alpha_region_content_footer_block_second_suffix' => '0',
      'alpha_region_content_footer_block_second_weight' => '0',
      'alpha_region_content_footer_block_second_position' => '0',
      'alpha_region_content_footer_block_second_css' => '',
      'alpha_region_content_footer_block_second_equal_height_element' => FALSE,
      'alpha_region_content_footer_block_second_equal_height_container' => 0,
      'alpha_region_content_footer_block_first_force' => 0,
      'alpha_region_content_footer_block_first_prefix' => '0',
      'alpha_region_content_footer_block_first_columns' => '1',
      'alpha_region_content_footer_block_first_suffix' => '0',
      'alpha_region_content_footer_block_first_weight' => '0',
      'alpha_region_content_footer_block_first_position' => '0',
      'alpha_region_content_footer_block_first_css' => '',
      'alpha_region_content_footer_block_first_equal_height_element' => FALSE,
      'alpha_region_content_footer_block_first_equal_height_container' => 0,
      'alpha_region_content_your_say_force' => 0,
      'alpha_region_content_your_say_prefix' => '0',
      'alpha_region_content_your_say_columns' => '1',
      'alpha_region_content_your_say_suffix' => '0',
      'alpha_region_content_your_say_weight' => '0',
      'alpha_region_content_your_say_position' => '0',
      'alpha_region_content_your_say_css' => '',
      'alpha_region_content_your_say_equal_height_element' => FALSE,
      'alpha_region_content_your_say_equal_height_container' => 0,
      'alpha_region_content_welcome_profile_force' => 0,
      'alpha_region_content_welcome_profile_prefix' => '0',
      'alpha_region_content_welcome_profile_columns' => '1',
      'alpha_region_content_welcome_profile_suffix' => '0',
      'alpha_region_content_welcome_profile_weight' => '0',
      'alpha_region_content_welcome_profile_position' => '0',
      'alpha_region_content_welcome_profile_css' => '',
      'alpha_region_content_welcome_profile_equal_height_element' => FALSE,
      'alpha_region_content_welcome_profile_equal_height_container' => 0,
      'alpha_region_sidebar_second_zone' => NULL,
      'alpha_region_sidebar_second_equal_height_element' => FALSE,
      'alpha_region_content_columns' => '7',
      'alpha_region_content_weight' => '1',
      'alpha_region_sidebar_first_columns' => '4',
      'alpha_region_sidebar_first_weight' => '2',
      'alpha_region_sidebar_first_css' => 'push-1',
    ),
  );
  $export['nhs_two_column_clean'] = $delta;

  return $export;
}
