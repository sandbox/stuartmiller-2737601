<?php
/**
 * @file
 * nhs_contact_us_page.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function nhs_contact_us_page_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nhs_contact_us';
  $context->description = 'Correctly theme the contact us page';
  $context->tag = 'nhs';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/7' => 'node/7',
      ),
    ),
  );
  $context->reactions = array(
    'delta' => array(
      'delta_template' => 'nhs_two_column_clean',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Correctly theme the contact us page');
  t('nhs');
  $export['nhs_contact_us'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'nhs_contact_us_map';
  $context->description = 'Correctly display the map on pages';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/7' => 'node/7',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-contact_us_map' => array(
          'module' => 'boxes',
          'delta' => 'contact_us_map',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'boxes-nhs_foi' => array(
          'module' => 'boxes',
          'delta' => 'nhs_foi',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Correctly display the map on pages');
  $export['nhs_contact_us_map'] = $context;

  return $export;
}
