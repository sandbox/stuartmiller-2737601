<?php
/**
 * @file
 * nhs_import_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function nhs_import_content_types_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function nhs_import_content_types_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function nhs_import_content_types_node_info() {
  $items = array(
    'condition' => array(
      'name' => t('Condition'),
      'base' => 'node_content',
      'description' => t('Store the name of the condition and the link to NHS Choices. In SOLR, if someone searches on \'Alcohol\' they should be able to fllter by \'Conditions\' (alongside what other
facets are available) where the results are the names of the conditions with the links to NHS Choices.
The description can be \'Find our more about $conditionname on NHS Choices\''),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'dentist' => array(
      'name' => t('Dentist'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'general_practitioner' => array(
      'name' => t('General practitioner'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'opticians' => array(
      'name' => t('Opticians'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'organisation' => array(
      'name' => t('Organisation'),
      'base' => 'node_content',
      'description' => t('A GP, Hospital etc'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'pharmacy' => array(
      'name' => t('Pharmacy'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'rating' => array(
      'name' => t('Rating'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'staff' => array(
      'name' => t('Staff'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  return $items;
}
