<?php
/**
 * @file
 * nhs_import_content_types.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function nhs_import_content_types_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'book_an_appointment_online';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Book an appointment Online';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Book an appointment Online';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_org_additional_data_ref_target_id']['id'] = 'field_org_additional_data_ref_target_id';
  $handler->display->display_options['relationships']['field_org_additional_data_ref_target_id']['table'] = 'field_data_field_org_additional_data_ref';
  $handler->display->display_options['relationships']['field_org_additional_data_ref_target_id']['field'] = 'field_org_additional_data_ref_target_id';
  /* Field: Content: Online Appointment Booking URL */
  $handler->display->display_options['fields']['field_online_appointment_booking']['id'] = 'field_online_appointment_booking';
  $handler->display->display_options['fields']['field_online_appointment_booking']['table'] = 'field_data_field_online_appointment_booking';
  $handler->display->display_options['fields']['field_online_appointment_booking']['field'] = 'field_online_appointment_booking';
  $handler->display->display_options['fields']['field_online_appointment_booking']['relationship'] = 'field_org_additional_data_ref_target_id';
  $handler->display->display_options['fields']['field_online_appointment_booking']['label'] = '';
  $handler->display->display_options['fields']['field_online_appointment_booking']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_online_appointment_booking']['alter']['text'] = 'Book an appointment online';
  $handler->display->display_options['fields']['field_online_appointment_booking']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_online_appointment_booking']['alter']['path'] = '[field_online_appointment_booking-url]';
  $handler->display->display_options['fields']['field_online_appointment_booking']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_online_appointment_booking']['click_sort_column'] = 'url';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['book_an_appointment_online'] = $view;

  $view = new view();
  $view->name = 'nhs_gp_accepting_new_patients';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'NHS GP Accepting New Patients';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Accepting new patients: ';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<div class="field-label">Accepting New Patients: No</div>';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* No results behavior: Global: View area */
  $handler->display->display_options['empty']['view']['id'] = 'view';
  $handler->display->display_options['empty']['view']['table'] = 'views';
  $handler->display->display_options['empty']['view']['field'] = 'view';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_org_additional_data_ref_target_id']['id'] = 'field_org_additional_data_ref_target_id';
  $handler->display->display_options['relationships']['field_org_additional_data_ref_target_id']['table'] = 'field_data_field_org_additional_data_ref';
  $handler->display->display_options['relationships']['field_org_additional_data_ref_target_id']['field'] = 'field_org_additional_data_ref_target_id';
  $handler->display->display_options['relationships']['field_org_additional_data_ref_target_id']['label'] = 'entity referenced from field_org_additional_data_ref';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="field-label">Accepting New Patients: Yes</div>
<div class=\'registration-form-download\'>Download a <a href=\'/gp-registration-form\'>registration form</a></div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Accepting new patients (field_gp_accepting_new_patients) */
  $handler->display->display_options['filters']['field_gp_accepting_new_patients_value']['id'] = 'field_gp_accepting_new_patients_value';
  $handler->display->display_options['filters']['field_gp_accepting_new_patients_value']['table'] = 'field_data_field_gp_accepting_new_patients';
  $handler->display->display_options['filters']['field_gp_accepting_new_patients_value']['field'] = 'field_gp_accepting_new_patients_value';
  $handler->display->display_options['filters']['field_gp_accepting_new_patients_value']['relationship'] = 'field_org_additional_data_ref_target_id';
  $handler->display->display_options['filters']['field_gp_accepting_new_patients_value']['value'] = 'We are currently accepting new patients';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['nhs_gp_accepting_new_patients'] = $view;

  return $export;
}
