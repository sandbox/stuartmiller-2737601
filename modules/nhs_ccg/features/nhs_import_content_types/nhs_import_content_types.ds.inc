<?php
/**
 * @file
 * nhs_import_content_types.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function nhs_import_content_types_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|organisation|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'organisation';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'appointment_booking' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:15:{s:23:"override_pager_settings";i:0;s:9:"use_pager";i:0;s:14:"nodes_per_page";s:1:"1";s:8:"pager_id";s:1:"0";s:6:"offset";s:1:"0";s:9:"more_link";i:0;s:10:"feed_icons";i:0;s:10:"panel_args";i:0;s:12:"link_to_view";i:0;s:4:"args";s:0:"";s:3:"url";s:0:"";s:7:"display";s:5:"block";s:7:"context";a:1:{i:0;s:29:"argument_entity_id:node_1.nid";}s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}s:4:"type";s:5:"views";s:7:"subtype";s:26:"book_an_appointment_online";}',
        'load_terms' => 0,
      ),
    ),
    'accepting_new_patients' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:15:{s:23:"override_pager_settings";i:0;s:9:"use_pager";i:0;s:14:"nodes_per_page";s:1:"5";s:8:"pager_id";s:1:"0";s:6:"offset";s:1:"0";s:9:"more_link";i:0;s:10:"feed_icons";i:0;s:10:"panel_args";i:0;s:12:"link_to_view";i:0;s:4:"args";s:0:"";s:3:"url";s:0:"";s:7:"display";s:5:"block";s:7:"context";a:1:{i:0;s:29:"argument_entity_id:node_1.nid";}s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}s:4:"type";s:5:"views";s:7:"subtype";s:29:"nhs_gp_accepting_new_patients";}',
        'load_terms' => 0,
      ),
    ),
  );
  $export['node|organisation|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|organisation|search_result';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'organisation';
  $ds_fieldsetting->view_mode = 'search_result';
  $ds_fieldsetting->settings = array(
    'appointment_booking' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:15:{s:23:"override_pager_settings";i:0;s:9:"use_pager";i:0;s:14:"nodes_per_page";s:1:"1";s:8:"pager_id";s:1:"0";s:6:"offset";s:1:"0";s:9:"more_link";i:0;s:10:"feed_icons";i:0;s:10:"panel_args";i:0;s:12:"link_to_view";i:0;s:4:"args";s:0:"";s:3:"url";s:0:"";s:7:"display";s:5:"block";s:7:"context";a:1:{i:0;s:29:"argument_entity_id:node_1.nid";}s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}s:4:"type";s:5:"views";s:7:"subtype";s:26:"book_an_appointment_online";}',
        'load_terms' => 0,
      ),
    ),
    'accepting_new_patients' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:15:{s:23:"override_pager_settings";i:0;s:9:"use_pager";i:0;s:14:"nodes_per_page";s:1:"5";s:8:"pager_id";s:1:"0";s:6:"offset";s:1:"0";s:9:"more_link";i:0;s:10:"feed_icons";i:0;s:10:"panel_args";i:0;s:12:"link_to_view";i:0;s:4:"args";s:0:"";s:3:"url";s:0:"";s:7:"display";s:5:"block";s:7:"context";a:1:{i:0;s:29:"argument_entity_id:node_1.nid";}s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}s:4:"type";s:5:"views";s:7:"subtype";s:29:"nhs_gp_accepting_new_patients";}',
        'load_terms' => 0,
      ),
    ),
    'single_line_address' => array(
      'weight' => '3',
      'label' => 'inline',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h2',
        'class' => '',
      ),
    ),
  );
  $export['node|organisation|search_result'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function nhs_import_content_types_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'accepting_new_patients';
  $ds_field->label = 'Accepting New Patients';
  $ds_field->field_type = 7;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = 'organisation|*';
  $ds_field->properties = array(
    'default' => array(),
    'settings' => array(
      'show_title' => array(
        'type' => 'checkbox',
      ),
      'title_wrapper' => array(
        'type' => 'textfield',
        'description' => 'Eg: h1, h2, p',
      ),
      'ctools' => array(
        'type' => 'ctools',
      ),
    ),
  );
  $export['accepting_new_patients'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'appointment_booking';
  $ds_field->label = 'Appointment booking';
  $ds_field->field_type = 7;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'default' => array(),
    'settings' => array(
      'show_title' => array(
        'type' => 'checkbox',
      ),
      'title_wrapper' => array(
        'type' => 'textfield',
        'description' => 'Eg: h1, h2, p',
      ),
      'ctools' => array(
        'type' => 'ctools',
      ),
    ),
  );
  $export['appointment_booking'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'single_line_address';
  $ds_field->label = 'Address';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = 'organisation|search_result';
  $ds_field->properties = array(
    'code' => array(
      'value' => '[node:field_org_address_line_1], [node:field_org_address_line_2], [node:field_org_address_line_3], [node:field_org_postcode]',
      'format' => 'plain_text',
    ),
    'use_token' => 1,
  );
  $export['single_line_address'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function nhs_import_content_types_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|organisation|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'organisation';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'right' => array(
        0 => 'appointment_booking',
        2 => 'field_org_address_line_1',
        3 => 'field_org_address_line_2',
        4 => 'group_org_overview',
        5 => 'field_org_address_line_3',
        6 => 'group_org_links',
        7 => 'accepting_new_patients',
        8 => 'field_org_address_line_4',
        9 => 'field_org_address_line_5',
        10 => 'field_org_postcode',
        11 => 'field_org_telephone',
        12 => 'field_org_fax',
        13 => 'field_org_website_url',
        14 => 'field_org_email',
        15 => 'field_org_perf_url',
        16 => 'field_org_ratings_url',
        17 => 'field_org_leave_review_url',
      ),
      'left' => array(
        1 => 'field_geo',
      ),
    ),
    'fields' => array(
      'appointment_booking' => 'right',
      'field_geo' => 'left',
      'field_org_address_line_1' => 'right',
      'field_org_address_line_2' => 'right',
      'group_org_overview' => 'right',
      'field_org_address_line_3' => 'right',
      'group_org_links' => 'right',
      'accepting_new_patients' => 'right',
      'field_org_address_line_4' => 'right',
      'field_org_address_line_5' => 'right',
      'field_org_postcode' => 'right',
      'field_org_telephone' => 'right',
      'field_org_fax' => 'right',
      'field_org_website_url' => 'right',
      'field_org_email' => 'right',
      'field_org_perf_url' => 'right',
      'field_org_ratings_url' => 'right',
      'field_org_leave_review_url' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|organisation|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|organisation|search_result';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'organisation';
  $ds_layout->view_mode = 'search_result';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title',
        2 => 'field_org_website_url',
        3 => 'single_line_address',
      ),
      'right' => array(
        1 => 'appointment_booking',
        4 => 'field_org_telephone',
        5 => 'accepting_new_patients',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'appointment_booking' => 'right',
      'field_org_website_url' => 'left',
      'single_line_address' => 'left',
      'field_org_telephone' => 'right',
      'accepting_new_patients' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|organisation|search_result'] = $ds_layout;

  return $export;
}
