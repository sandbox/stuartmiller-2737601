<?php
/**
 * @file
 * nhs_import_content_types.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function nhs_import_content_types_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_am_hours|field_collection_item|field_general_opening_times|form';
  $field_group->group_name = 'group_am_hours';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_general_opening_times';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'AM',
    'weight' => '1',
    'children' => array(
      0 => 'field_gen_from',
      1 => 'field_gen_to',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_am_hours|field_collection_item|field_general_opening_times|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_contact_details|node|organisation|form';
  $field_group->group_name = 'group_contact_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_ht';
  $field_group->data = array(
    'label' => 'Contact details',
    'weight' => '3',
    'children' => array(
      0 => 'field_org_address_line_1',
      1 => 'field_org_address_line_2',
      2 => 'field_org_address_line_3',
      3 => 'field_org_address_line_4',
      4 => 'field_org_address_line_5',
      5 => 'field_org_email',
      6 => 'field_org_fax',
      7 => 'field_org_postcode',
      8 => 'field_org_telephone',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Contact details',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_contact_details|node|organisation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ht|node|organisation|form';
  $field_group->group_name = 'group_ht';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Horizontal Tabs',
    'weight' => '0',
    'children' => array(
      0 => 'group_contact_details',
      1 => 'group_location',
      2 => 'group_overview',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_ht|node|organisation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_location|node|organisation|form';
  $field_group->group_name = 'group_location';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_ht';
  $field_group->data = array(
    'label' => 'Location',
    'weight' => '4',
    'children' => array(
      0 => 'field_geo',
      1 => 'field_org_latitude',
      2 => 'field_org_longitude',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Location',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_location|node|organisation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_org_links|node|organisation|default';
  $field_group->group_name = 'group_org_links';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'More information from NHS Choices',
    'weight' => '3',
    'children' => array(
      0 => 'field_org_leave_review_url',
      1 => 'field_org_perf_url',
      2 => 'field_org_ratings_url',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'More information from NHS Choices',
      'instance_settings' => array(
        'classes' => '',
        'description' => '(These links will take you to a different website)',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_org_links|node|organisation|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_org_overview|node|organisation|default';
  $field_group->group_name = 'group_org_overview';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Overview',
    'weight' => '2',
    'children' => array(
      0 => 'field_org_address_line_1',
      1 => 'field_org_address_line_2',
      2 => 'field_org_address_line_3',
      3 => 'field_org_address_line_4',
      4 => 'field_org_address_line_5',
      5 => 'field_org_email',
      6 => 'field_org_fax',
      7 => 'field_org_postcode',
      8 => 'field_org_telephone',
      9 => 'field_org_website_url',
      10 => 'appointment_booking',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Overview',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_org_overview|node|organisation|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_overview|node|organisation|form';
  $field_group->group_name = 'group_overview';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_ht';
  $field_group->data = array(
    'label' => 'Overview',
    'weight' => '2',
    'children' => array(
      0 => 'field_org_additional_data_ref',
      1 => 'field_org_id',
      2 => 'field_org_leave_review_url',
      3 => 'field_org_nhs_choices_url',
      4 => 'field_org_ods_code',
      5 => 'field_org_parent_org_url',
      6 => 'field_org_parent_organisation',
      7 => 'field_org_perf_url',
      8 => 'field_org_ratings_url',
      9 => 'field_org_thumbnail',
      10 => 'field_org_website_url',
      11 => 'field_org_type',
      12 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Overview',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_overview|node|organisation|form'] = $field_group;

  return $export;
}
