<?php
/**
 * @file
 * nhs_home_page.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function nhs_home_page_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'nhs_home_blocks';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'NHS Home Blocks';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'NHS Home Blocks';
  $handler->display->display_options['css_class'] = 'have-your-say';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Welcome Content */
  $handler = $view->new_display('block', 'Welcome Content', 'block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Photo */
  $handler->display->display_options['fields']['field_home_profile_photo']['id'] = 'field_home_profile_photo';
  $handler->display->display_options['fields']['field_home_profile_photo']['table'] = 'field_data_field_home_profile_photo';
  $handler->display->display_options['fields']['field_home_profile_photo']['field'] = 'field_home_profile_photo';
  $handler->display->display_options['fields']['field_home_profile_photo']['label'] = '';
  $handler->display->display_options['fields']['field_home_profile_photo']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_home_profile_photo']['element_class'] = 'home-profile-photo left';
  $handler->display->display_options['fields']['field_home_profile_photo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_home_profile_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_home_profile_photo']['settings'] = array(
    'image_style' => 'nhs_home_profile_image',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Signature */
  $handler->display->display_options['fields']['field_home_profile_signature']['id'] = 'field_home_profile_signature';
  $handler->display->display_options['fields']['field_home_profile_signature']['table'] = 'field_data_field_home_profile_signature';
  $handler->display->display_options['fields']['field_home_profile_signature']['field'] = 'field_home_profile_signature';
  $handler->display->display_options['fields']['field_home_profile_signature']['label'] = '';
  $handler->display->display_options['fields']['field_home_profile_signature']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_home_profile_signature']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_home_profile_signature']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_home_profile_signature']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Position */
  $handler->display->display_options['fields']['field_home_profile_position']['id'] = 'field_home_profile_position';
  $handler->display->display_options['fields']['field_home_profile_position']['table'] = 'field_data_field_home_profile_position';
  $handler->display->display_options['fields']['field_home_profile_position']['field'] = 'field_home_profile_position';
  $handler->display->display_options['fields']['field_home_profile_position']['label'] = '';
  $handler->display->display_options['fields']['field_home_profile_position']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_home_profile_position']['element_label_colon'] = FALSE;
  /* Field: Content: Name */
  $handler->display->display_options['fields']['field_home_profile_name']['id'] = 'field_home_profile_name';
  $handler->display->display_options['fields']['field_home_profile_name']['table'] = 'field_data_field_home_profile_name';
  $handler->display->display_options['fields']['field_home_profile_name']['field'] = 'field_home_profile_name';
  $handler->display->display_options['fields']['field_home_profile_name']['label'] = '';
  $handler->display->display_options['fields']['field_home_profile_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_home_profile_name']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_home_profile_name']['alter']['text'] = '<p><strong>[field_home_profile_name-value]</strong> [field_home_profile_position]</p>';
  $handler->display->display_options['fields']['field_home_profile_name']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<h2>[title]</h2>
[body]
[field_home_profile_signature]
[field_home_profile_name]';
  $handler->display->display_options['fields']['nothing']['element_type'] = 'div';
  $handler->display->display_options['fields']['nothing']['element_class'] = 'brighter-healthier overflow';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'nhs_home_page' => 'nhs_home_page',
  );

  /* Display: Your Say Block */
  $handler = $view->new_display('block', 'Your Say Block', 'block_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'grid-4 omega';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = 'Have your say';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Have your say */
  $handler->display->display_options['fields']['field_home_have_your_say']['id'] = 'field_home_have_your_say';
  $handler->display->display_options['fields']['field_home_have_your_say']['table'] = 'field_data_field_home_have_your_say';
  $handler->display->display_options['fields']['field_home_have_your_say']['field'] = 'field_home_have_your_say';
  $handler->display->display_options['fields']['field_home_have_your_say']['label'] = '';
  $handler->display->display_options['fields']['field_home_have_your_say']['element_label_colon'] = FALSE;
  /* Field: Content: Link */
  $handler->display->display_options['fields']['field_home_have_your_say_link']['id'] = 'field_home_have_your_say_link';
  $handler->display->display_options['fields']['field_home_have_your_say_link']['table'] = 'field_data_field_home_have_your_say_link';
  $handler->display->display_options['fields']['field_home_have_your_say_link']['field'] = 'field_home_have_your_say_link';
  $handler->display->display_options['fields']['field_home_have_your_say_link']['label'] = '';
  $handler->display->display_options['fields']['field_home_have_your_say_link']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_home_have_your_say_link']['element_class'] = 'have-your-say-href';
  $handler->display->display_options['fields']['field_home_have_your_say_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_home_have_your_say_link']['click_sort_column'] = 'url';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'nhs_home_page' => 'nhs_home_page',
  );
  $export['nhs_home_blocks'] = $view;

  return $export;
}
