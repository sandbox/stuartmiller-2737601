<?php
/**
 * @file
 * nhs_home_page.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function nhs_home_page_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_home_have_your_say|node|nhs_home_page|form';
  $field_group->group_name = 'group_home_have_your_say';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'nhs_home_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Have Your Say',
    'weight' => '43',
    'children' => array(
      0 => 'field_home_have_your_say',
      1 => 'field_home_have_your_say_link',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_home_have_your_say|node|nhs_home_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_home_welcome|node|nhs_home_page|form';
  $field_group->group_name = 'group_home_welcome';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'nhs_home_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Welcome Information',
    'weight' => '42',
    'children' => array(
      0 => 'body',
      1 => 'field_home_profile_photo',
      2 => 'field_home_profile_signature',
      3 => 'field_home_profile_name',
      4 => 'field_home_profile_position',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_home_welcome|node|nhs_home_page|form'] = $field_group;

  return $export;
}
