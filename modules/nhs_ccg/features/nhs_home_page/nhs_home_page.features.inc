<?php
/**
 * @file
 * nhs_home_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function nhs_home_page_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function nhs_home_page_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function nhs_home_page_image_default_styles() {
  $styles = array();

  // Exported image style: nhs_home_profile_image.
  $styles['nhs_home_profile_image'] = array(
    'name' => 'nhs_home_profile_image',
    'effects' => array(
      7 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 157,
          'height' => 272,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function nhs_home_page_node_info() {
  $items = array(
    'nhs_home_page' => array(
      'name' => t('NHS Home page'),
      'base' => 'node_content',
      'description' => t('Use <i>NHS Home page</i> to display front page content only'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
