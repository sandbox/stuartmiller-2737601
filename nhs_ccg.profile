<?php

/**
 * Implements hook_install_tasks().
 */
function nhs_ccg_install_tasks($install_state) {

  $tasks = array(
    'initial_content' => array(
      'display_name' => st('Install initial content?'),
      'type' => 'form',
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
      'function' => 'install_initial_content_form',
    ),
    'site_install_finish' => array(
      'display_name' => 'Finish install',
      'display' => false,
      'type' => 'normal'
    ),
  );
  return $tasks;
}

function install_initial_content_form($form, &$form_state) {
  $form['install_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Install initial content'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Continue',
  );

  return $form;
}

function install_initial_content_form_submit($form, &$form_state) {
  if ($form_state['values']['install_content'] == 1) {
    module_enable(array('ccg_demo'), true);
  }
}

function site_install_finish() {
  features_revert();
}
