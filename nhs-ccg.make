core: "7.x"
api: 2
projects:
  drupal:
    version: ~
  addthis:
    subdir: contrib
    version: 4.0-alpha6
  admin_menu:
    subdir: contrib
    version: 3.0-rc5
  admin_views:
    subdir: contrib
    version: '1.5'
  advuser:
    subdir: contrib
    version: 3.0-beta1
  auto_username:
    subdir: contrib
    version: 1.0-alpha2
  bassline:
      type: theme
      version: '1.3'
  boost:
    subdir: contrib
    version: '1.0'
  boxes:
    subdir: contrib
    version: '1.2'
  bu:
    subdir: contrib
    version: '1.3'
  ctools:
    subdir: contrib
    version: '1.9'
  color_field:
    subdir: contrib
    version: '1.8'
  conditional_fields:
    subdir: contrib
    version: '3.0-alpha2'
  context:
    subdir: contrib
    version: '3.6'
  facetapi:
    subdir: contrib
    version: '1.5'
  date:
    subdir: contrib
    version: '2.9'
  delta:
    subdir: contrib
    version: 3.0-beta11
  disable_node_menu_item:
    subdir: contrib
    version: '1.1'
  ds:
    subdir: contrib
    version: '2.13'
  eck:
    subdir: contrib
    version: '2.0-rc8'
  email:
    subdir: contrib
    version: '1.3'
  entity:
    subdir: contrib
    version: '1.6'
  entityreference:
    subdir: contrib
    version: '1.1'
  eu_cookie_compliance:
    subdir: contrib
    version: '1.14'
  facetapi_bonus:
    subdir: contrib
    version: '1.1'
  facetapi_pretty_paths:
    subdir: contrib
    version: '1.4'
  features:
    subdir: contrib
    version: '2.7'
  field_collection:
    subdir: contrib
    version: 1.0-beta11
  field_group:
    subdir: contrib
    version: '1.5'
  file_entity:
    subdir: contrib
    version: 2.0-beta2
  flexslider:
    subdir: contrib
    version: 2.0-rc1
  flexslider_entityreference:
    subdir: contrib
    version: '1.6'
  geofield:
    subdir: contrib
    version: '2.3'
  geophp:
    subdir: contrib
    version: '1.7'
  globalredirect:
    subdir: contrib
    version: '1.5'
  google_analytics:
    subdir: contrib
    version: '1.3'
  honeypot:
    subdir: contrib
    version: '1.22'
  inline_entity_form:
    subdir: contrib
    version: '1.8'
  libraries:
    subdir: contrib
    version: '2.2'
  jquery_update:
    subdir: contrib
    version: '2.7'
  link:
    subdir: contrib
    version: '1.4'
  linkit:
    subdir: contrib
    version: '3.5'
  logintoboggan:
    subdir: contrib
    version: '1.5'
  media:
    subdir: contrib
    version: 2.0-beta1
  media_youtube:
    subdir: contrib
    version: '3.0'
  media_vimeo:
    subdir: contrib
    version: '2.1'
  menu_attributes:
    subdir: contrib
    version: 1.0-rc3
  menu_block:
    subdir: contrib
    version: '2.7'
  metatag:
    subdir: contrib
    version: '1.0-beta2'
  migrate:
      subdir: contrib
      version: '2.8'
  module_filter:
    subdir: contrib
    version: '2.0'
  node_comment_block:
    subdir: contrib
    version: '1.1'
  node_expire:
    subdir: contrib
    version: '1.8'
  password_policy:
    subdir: contrib
    version: '1.12'
  pathauto:
    subdir: contrib
    version: '1.3'
  pathologic:
    subdir: contrib
    version: '3.1'
  prepro:
    subdir: contrib
    version: '1.4'
  redirect:
    subdir: contrib
    version: 1.0-rc3
  reroute_email:
    subdir: contrib
    version: '1.2'
  respondjs:
    subdir: contrib
    version: '1.5'
  rules:
    subdir: contrib
    version: '2.9'
  sassy:
    subdir: contrib
    version: '2.17'
  seckit:
    subdir: contrib
    version: '1.9'
  shield:
    subdir: contrib
    version: '1.2'
  simple_dialog:
    subdir: contrib
    version: '1.10'
  site_map:
    subdir: contrib
    version: '1.3'
  site_verify:
    subdir: contrib
    version: '1.1'
  socialmedia:
    subdir: contrib
    version: 1.0-beta15
  strongarm:
    subdir: contrib
    version: '2.0'
  token:
    subdir: contrib
    version: '1.6'
  transliteration:
    subdir: contrib
    version: '3.2'
  twitter_block:
    subdir: contrib
    version: '2.3'
  username_enumeration_prevention:
    subdir: contrib
    version: '1.2'
  views:
    subdir: contrib
    version: '3.13'
  views_bulk_operations:
    subdir: contrib
    version: '3.3'
  views_data_export:
    subdir: contrib
    version: '3.0-beta9'
  view_unpublished:
    subdir: contrib
    version: '1.2'
  webform:
    subdir: contrib
    version: '4.12'
  weight:
    subdir: contrib
    version: '3.1'
  widgets:
    subdir: contrib
    version: 1.0-rc1
  wysiwyg:
    subdir: contrib
    version: '2.x-dev'
  xmlsitemap:
    subdir: contrib
    version: '2.2'
  omega:
    version: '3.1'
libraries:
  ckeditor:
    directory_name: ckeditor
    custom_download: true
    type: library
    destination: libraries
    directory_name: ckeditor
    download:
      type: get
      url: http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.5.9/ckeditor_4.5.9_standard.tar.gz
  flexslider:
    directory_name: flexslider
    custom_download: true
    type: library
    download:
      url: https://github.com/woothemes/FlexSlider/archive/2.6.0.zip
